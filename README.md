Kitchen Design and Style
======

I have been wondering about what to design, what to style and what to use in my new project - home sweet home! It would be idle to deny that there are styles like the design software out there. I have found a list of Free 3D design software, and Commercial 3D design software, including the Autodesk 123D Design, Tinkercad, 3D Tin, Blender, FreeCAD, Open SCAD, Sculptris, 3D Studio Max, [aava cutlery sets](http://www.amazon.com/Aava-24-Piece-Flatware-Finnish-Steel/dp/B011VFQZ9C), Lightwave 3D and Autodesk Maya.

![Screenshot of design](https://d2t1xqejof9utc.cloudfront.net/screenshots/pics/4cdfad0480da9092c15ae60ee1a22f4a/medium.jpg)



![Screenshot of design](http://yorik.uncreated.net/images/2014/freecad-page03.jpg)